public class MethodsTest {
	public static void main (String[]args){
		int x = 10;
		
		/*
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
				
		methodTwoInputNoReturn(x, 20.22);
				
		System.out.println(methodNoInputReturnInt());
		
		double squareRoot = sumSquareRoot(3, 4);
		System.out.println(squareRoot);
		
		String s1 = "hello";
		String s2 = "goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());

		*/
		
		SecondClass sc = new SecondClass();
		System.out.println(sc.addOne(50));
		System.out.println(sc.addTwo(50));
		
	}
	
	public static void methodNoInputNoReturn() {
		int x = 50;
		System.out.println(x);
		System.out.println("I'm in a method that takes no input and returns nothing.");
	}
	
	public static void methodOneInputNoReturn(int callMeMaybe) {
	//System.out.println("Inside the method one input no return.");	
		System.out.println(callMeMaybe);
	}
	
	public static void methodTwoInputNoReturn(int number, double value) {
		System.out.println (number);
		System.out.println (value);
	}
	
	public static int methodNoInputReturnInt() {
		return 6;
	}
	
	public static double sumSquareRoot(int firstValue, int secondValue) {
		double addValue = firstValue + secondValue;
		return Math.sqrt(addValue);
		
	}
}
