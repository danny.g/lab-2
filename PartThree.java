import java.util.Scanner;
public class PartThree {
	public static void main (String[]args) {
		Scanner sc = new Scanner (System.in);
		AreaComputations area = new AreaComputations();
		
		System.out.println("Enter a length for the square.");
		double sqrLength = sc.nextDouble();
		System.out.println("Enter a length for the rectangle.");
		double recLength = sc.nextDouble();
		System.out.println("Enter a width for the rectangle.");
		double recWidth = sc.nextDouble();
		
		double areaSqr = area.areaSquare(sqrLength);
		double areaRec = area.areaRectangle(recLength, recWidth);
		
		System.out.println ("The area of the square is: " + areaSqr + " and the area of the rectangle is: " + areaRec + ".");
	}
}